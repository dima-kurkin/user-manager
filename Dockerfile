ARG PYTHON_IMAGE=3.8-alpine
# ===============================================================================================================
# Build Stage 0
# ===============================================================================================================
FROM python:${PYTHON_IMAGE} AS base-image

RUN apk update --no-progress && \
    apk add --no-cache --no-progress ca-certificates openssl wget

# Add logger
RUN wget -q http://github.com/papertrail/remote_syslog2/releases/download/v0.20/remote_syslog_linux_amd64.tar.gz && \
    tar -zxvf remote_syslog_linux_amd64.tar.gz && \
    cp ./remote_syslog/remote_syslog /usr/local/bin && \
    rm -f remote_syslog_linux_amd64.tar.gz && \
    apk del --no-progress wget && \
    rm -rf /var/cache/apk/*

# ensure local remote_syslog is preferred over install remote_syslog
ENV PATH /usr/local/bin:$PATH

# ===============================================================================================================
# Build Stage 1
# ===============================================================================================================
FROM python:${PYTHON_IMAGE} AS base-build

RUN apk update && \
    apk --no-cache --update add \
    build-base \
    python3-dev \
    openssl-dev \
    libffi-dev \
    gcc

RUN pip3 install --upgrade pip

ENV PATH /root/.local/bin:$PATH

# ===============================================================================================================
# Build Stage 2.1 - Image Common Build
# ===============================================================================================================
FROM base-build AS common-build

# Common Locked Requirements
ADD common/requirements.locked.txt .
RUN pip3 install --no-color --quiet --user  -r requirements.locked.txt
# ===============================================================================================================
# Build Stage 2.2 - Image Service Build
# ===============================================================================================================
FROM base-build AS service-build

# Locked requirements
ADD requirements.locked.txt .
RUN pip3 install --no-color --quiet --user -r requirements.locked.txt
# ===============================================================================================================
# Build Stage 3 - Image Service
# ===============================================================================================================
FROM base-image AS service-image

# Switches to a non-root user
RUN addgroup -S mayple && adduser -S maypleapp -G mayple --home /home/maypleapp

USER maypleapp
WORKDIR /home/maypleapp

# Copy the package from Stage 2.1 & 2.2
COPY --chown=maypleapp:mayple --from=common-build /root/.local /home/maypleapp/.local
COPY --chown=maypleapp:mayple --from=service-build /root/.local /home/maypleapp/.local

# Make sure scripts in .local are usable
ENV PATH=/home/maypleapp/.local/bin:$PATH
# Prevents Python from writing pyc files to disc
ENV PYTHONDONTWRITEBYTECODE 1

# Add source files
ADD common/src/common common
ADD src .
ADD alembic alembic

# =====================================
# Microservice Specific
# =====================================

# Convox assembles these during `convox start` and deployment
ENV LINK_SCHEME http
ENV LINK_PORT 11211

# Default port - recommend selecting port 1024 or above to mitigate security concerns from running as a root user
EXPOSE 11211

ARG PERFPIE_ENVIRONMENT=dorky
# Received during the run time process - The env can be override outside the docker
ENV SYSLOG_SERVER_NAME=logs6.papertrailapp.com
ENV SYSLOG_SERVER_PORT=41353

# Add logger
ENV SERVICE_NAME boilerplate

# Main Command
ENTRYPOINT remote_syslog --tls -d $SYSLOG_SERVER_NAME -p $SYSLOG_SERVER_PORT --hostname $PERFPIE_ENVIRONMENT $SERVICE_NAME=/tmp/output.log; \
     python3 -O $SERVICE_NAME.py 2>&1 | tee /tmp/output.log