"""
Accounts service.
"""

# Copyright 2017-2019 (c) Mayple. All rights reserved.
# Copying and/or distribution of this file is prohibited.
import logging
from typing import Dict

from flask_sqlalchemy_session import current_session

from common.api import EntityCreateResponse
from common.api import EntityDisableResponse
from common.api import EntityGetResponse
from common.api import EntityListResponse
from common.api import EntityUpdateResponse
from common.api.auth import VerifyAuthentication
from common.api.auth import VerifyAuthorization
from common.mysql import autoCommitter
from common.mysql import disableEntity
from common.mysql import getEntity
from common.mysql import getEntityList
from common.mysql import updateEntity
from logic.accounts import createAccountEntity
from models import Enumerations
from models.accounts import Account


# =========================================================================
# API Methods
# =========================================================================


@VerifyAuthentication
@VerifyAuthorization(Enumerations.UserType.SUPPORT_USER)
@EntityListResponse(Account)
def getAccountList() -> Dict:
    """
    Returns a list containing Account objects, optionally with a search query.
    The list supports paging and sorting.

    :return:
    """

    # Example coed to print log
    logging.info(f"Got request to get account list")

    return getEntityList(current_session, Account)


@VerifyAuthentication
@VerifyAuthorization(Enumerations.UserType.NORMAL_USER)
@EntityCreateResponse(Account)
def createAccount(
    accountCreate: Dict,
) -> Account:
    """
    Creates a new Account
    """

    # Example coed to print log
    logging.info(f"Got request to add account, info {accountCreate}")

    with autoCommitter(current_session) as session:
        return createAccountEntity(
            session=session,
            accountCreate=accountCreate,
        )


@VerifyAuthentication
@VerifyAuthorization(Enumerations.UserType.NORMAL_USER)
@EntityGetResponse(Account)
def getAccount(
    accountUuid: str,
) -> Account:
    """
    Returns a single Account based on its ID.
    """

    return getEntity(current_session, Account, accountUuid)


@VerifyAuthentication
@VerifyAuthorization(Enumerations.UserType.NORMAL_USER)
@EntityUpdateResponse(Account)
def updateAccount(
    accountUuid: str,
    accountUpdate: Dict,
) -> Account:
    """
    Updates a Account based on its ID.
    """

    with autoCommitter(current_session) as session:
        return updateEntity(
            session,
            Account,
            accountUuid,
            accountUpdate,
        )


@VerifyAuthentication
@VerifyAuthorization(Enumerations.UserType.SUPPORT_USER)
@EntityDisableResponse(Account)
def disableAccount(
    accountUuid: str,
) -> Account:
    """
    Disable a single Account identified via its ID
    :param accountUuid:
    :return:
    """

    with autoCommitter(current_session) as session:
        return disableEntity(session, Account, accountUuid)
