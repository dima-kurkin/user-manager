"""

Jobs related to this microservice. Scheduling does not happen here.

"""

# Copyright 2017-2019 (c) Mayple. All rights reserved.
# Copying and/or distribution of this file is prohibited.

import logging


#=======================================================================================================================
# Private Members
#=======================================================================================================================

#=======================================================================================================================
# Public Members
#=======================================================================================================================

def performAction():
    """
    Performs action

    :return:
    """

    try:

        pass

    except Exception:

        logging.exception('Action performing job failed.')
