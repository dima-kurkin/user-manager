"""

Simple internal scheduler for jobs inside this microservice. NOT meant for actual production work.
"""

# Copyright 2017-2019 (c) Mayple. All rights reserved.
# Copying and/or distribution of this file is prohibited.

import logging
import gevent
from datetime import datetime

from jobs import performAction


#=======================================================================================================================
# Public Members
#=======================================================================================================================


def scheduleJobs():

    """

    Schedule all jobs to run periodically.

    :return:
    """

    gevent.spawn_later(2, scheduleAction)

def scheduleAction():

    while True:
        startTime = datetime.utcnow()

        performAction()

        finishTime = datetime.utcnow()
        secondsToSleep = max(60 - (finishTime - startTime).total_seconds(), 0)
        logging.debug("Sleeping %s seconds until next upkeep.", secondsToSleep)
        gevent.sleep(secondsToSleep)
        