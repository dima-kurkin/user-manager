"""
Boilerplate's database models. In this file: accounts.
"""

# Copyright 2017-2019 (c) Mayple. All rights reserved.
# Copying and/or distribution of this file is prohibited.

from babel import Locale
from sqlalchemy.dialects.mysql import ENUM
from sqlalchemy.dialects.mysql import JSON
from sqlalchemy.dialects.mysql import VARCHAR
from sqlalchemy_utils import LocaleType
from sqlalchemy_utils import TimezoneType

from common.mysql import DatabaseEntity2
from common.mysql import Field
from common.mysql import FieldBehavior
from common.mysql import FieldReadPermission
from common.mysql import FieldRules
from common.mysql import getStringToEnumConversionFunc
from common.mysql import mergeJSONField
from models import Enumerations


# ------------------
# Account
# ------------------

class Account(DatabaseEntity2):
    """
    Accounts are the main access point to the system.
    """

    # =========================================================================
    # Private Methods
    # =========================================================================

    # MySQL table that contains this entity
    __tablename__ = 'accounts'
    # Set the table Identifier for update
    _entityIdentifierColumnName = 'uuid'
    # =========================================================================
    # Public Interface
    # =========================================================================

    emailAddress: str = Field(
        FieldRules(
            behavior=FieldBehavior.SET_ALWAYS,
            readPermission=FieldReadPermission.REQUIRES_VIEW_ACCESS_OR_BETTER,
            includeInDebugString=True,
        ),
        'emailAddress', VARCHAR(length=254),
        nullable=False,
    )

    accountType: Enumerations.UserType = Field(
        FieldRules(
            behavior=FieldBehavior.READ_ONLY,
            readPermission=FieldReadPermission.REQUIRES_VIEW_ACCESS_OR_BETTER,
            conversionFunc=getStringToEnumConversionFunc(Enumerations.UserType),
            includeInDebugString=True,
        ),
        'accountType', ENUM(Enumerations.UserType),
        nullable=False,
        default=Enumerations.UserType.NORMAL_USER,
        server_default=Enumerations.UserType.NORMAL_USER.name,
    )
    presentableName = Field(
        FieldRules(
            behavior=FieldBehavior.SET_ALWAYS,
            readPermission=FieldReadPermission.REQUIRES_VIEW_ACCESS_OR_BETTER,
            includeInDebugString=True,
        ),
        'presentableName', VARCHAR(length=180),
        nullable=False,
    )

    timezone = Field(
        FieldRules(
            behavior=FieldBehavior.READ_ONLY,
            readPermission=FieldReadPermission.REQUIRES_FULL_ACCESS_OR_BETTER,
            conversionFunc=mergeJSONField,
        ),
        'timezone', TimezoneType(backend='pytz'),
        default="UTC", server_default="UTC",
    )
    locale = Field(
        FieldRules(
            behavior=FieldBehavior.READ_ONLY,
            readPermission=FieldReadPermission.REQUIRES_FULL_ACCESS_OR_BETTER,
            conversionFunc=mergeJSONField,
        ),
        'locale', LocaleType,
        default=Locale('en_US'), server_default="en_US",
    )

