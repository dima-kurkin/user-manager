"""
Boilerplate's database models. In this file: database related code, including database definition and SQLAlchemy models.
"""

# Copyright 2017-2019 (c) Mayple. All rights reserved.
# Copying and/or distribution of this file is prohibited.

from common.mysql import EnumerationsBase


#=========================================================================
# Module Methods
#=========================================================================

#------------------------
# Association Tables
#------------------------

class AssociationTables:
    pass


#------------------
# Enumerations
#------------------

class Enumerations(EnumerationsBase):
    pass
