"""
This file should import all models to allow Alembic to properly create migrations.
"""

# Copyright 2017-2019 (c) Mayple. All rights reserved.
# Copying and/or distribution of this file is prohibited.

from models.accounts import Account

##############################################################################################
# DON'T FORGET TO UPDATE tests.fixtures.helpers.truncateDatabase() WHEN ADDING A NEW MODEL ! #
##############################################################################################

allModels = [
    Account,
]

##############################################################################################
# DON'T FORGET TO UPDATE tests.fixtures.helpers.truncateDatabase() WHEN ADDING A NEW MODEL ! #
##############################################################################################
