# MySQL database configuration

# Copyright 2017-2019 (c) Mayple. All rights reserved.
# Copying and/or distribution of this file is prohibited.

import os

# Convox specific: these are imported by Convox from the MySQL container. They can also be later on set via a resource.
MYSQL_HOST = os.environ.get('MYSQL_HOST') or 'localhost'
MYSQL_PORT = int(os.environ.get('MYSQL_PORT') or 3306)
MYSQL_USERNAME = os.environ.get('MYSQL_USERNAME') or 'root'
MYSQL_PASSWORD = os.environ.get('MYSQL_PASSWORD') or 'perfpie'
MYSQL_DB_NAME = 'boilerplate'

mySQLHost = MYSQL_HOST
mySQLPort = MYSQL_PORT
mySQLUsername = MYSQL_USERNAME
mySQLPassword = MYSQL_PASSWORD
mySQLDBName = MYSQL_DB_NAME
