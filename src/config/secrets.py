# Secrets configuration

# Copyright 2017-2019 (c) Mayple. All rights reserved.
# Copying and/or distribution of this file is prohibited.

import os

JWT_SECRET = os.environ.get('JWT_SECRET') or 'super-secret-jwt-key'

jwtSecret = JWT_SECRET