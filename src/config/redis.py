# Copyright 2017-2019 (c) Mayple. All rights reserved.
# Copying and/or distribution of this file is prohibited.

import os

# Convox specific: these are imported by Convox from the Redis container. They can also be later on set via a resource.
REDIS_HOST = os.environ.get('REDIS_HOST') or 'localhost'
REDIS_PORT = int(os.environ.get('REDIS_PORT') or 6379)
BOILERPLATE_REDIS_DB_NUMBER = int(os.environ.get('BOILERPLATE_REDIS_DB_NUMBER') or 15)

redisEndpoints = {
    "boilerplate" : (REDIS_HOST, REDIS_PORT, BOILERPLATE_REDIS_DB_NUMBER),
}
