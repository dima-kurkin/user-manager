# Copyright 2017-2019 (c) Mayple. All rights reserved.
# Copying and/or distribution of this file is prohibited.

from dotenv import load_dotenv, find_dotenv

print("Trying to load environment variables from .env file...")
load_dotenv(find_dotenv())
