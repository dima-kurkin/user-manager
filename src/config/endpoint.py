# Endpoint configuration

# Copyright 2017-2019 (c) Mayple. All rights reserved.
# Copying and/or distribution of this file is prohibited.

import os

INTERNAL_HOST = os.environ.get('INTERNAL_HOST') or '0.0.0.0'

# Convox specific: LINK_PORT is also exported from this microservice.
INTERNAL_PORT = int(os.environ.get('LINK_PORT') or os.environ.get('INTERNAL_PORT') or 11211)

internalHost: str = INTERNAL_HOST
internalPort: int = INTERNAL_PORT
