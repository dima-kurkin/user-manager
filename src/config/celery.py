import os

from common.utils.microservice import CeleryConfiguration


configuration: CeleryConfiguration = CeleryConfiguration(
    brokerUrl=os.getenv("CELERY_BROKER_URL", None),
    # The current param is optional, the task result is store there (should be used only in case result is required)
    backendUrl=os.getenv("CELERY_BACKEND_URL", None)  # The configuration should be Redis should add HA in AWS
)
