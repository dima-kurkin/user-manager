# Copyright 2017-2019 (c) Mayple. All rights reserved.
# Copying and/or distribution of this file is prohibited.
import logging

from celery import shared_task


@shared_task(
    bind=True,
    name="task.boilerplate.example",  # convention is - task.[service-name].[task-description]
    autoretry_for=(ZeroDivisionError, IndexError),
    retry_kwargs={'max_retries': 10},
    retry_backoff=5,  # Autoretries will be delayed following the rules of exponential backoff
    retry_jitter=False,
    task_time_limit=180,
    ignore_result=True
)
def handle_example(self, taskPayload: dict) -> None:
    """
    Code example of producer publish message:

    from common.celery.producer import CeleryProducers
    from common.celery.producer import bindCeleryPayload

    from uuid import uuid4 as uuid
    from pydantic import BaseModel

    @bindCeleryPayload(taskName="task.boilerplate.example")
    class ServiceTaskExamplePayload(BaseModel):
        name: str

    producer = CeleryProducers.get()
    producer.asyncSendBindTask(correlationId=uuid().hex, taskBindPayload=ServiceTaskExamplePayload(name='test'))

    """
    logging.info(f"Call [{self.request.correlation_id}] to handle message example with following params: {taskPayload}")
