# Copyright 2017-2019 (c) Mayple. All rights reserved.
# Copying and/or distribution of this file is prohibited.

from celery import shared_task


@shared_task(bind=True,name="boilerplate.schedules.projectMaintenance")
def execute_maintenance_job(self,name:str,count:int):
    print(f"Schedule task - {name} ({count})")