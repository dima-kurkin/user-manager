"""
Main implementation of the microservice.
"""

# Copyright 2017-2019 (c) Mayple. All rights reserved.
# Copying and/or distribution of this file is prohibited.

from celery.schedules import crontab

import config.endpoint
import config.mysql
import config.redis
import config.secrets
from common.config import celery
from common.utils.microservice import AbstractMicroservice
from common.utils.microservice import DatabaseConfiguration
from common.utils.microservice import RedisConfiguration
from common.utils.microservice import ScheduleMicroservice
from common.utils.microservice import ServiceEndpointConfiguration
from common.utils.microservice import TaskQueueMicroservice
from common.utils.microservice import getExecutionServiceByConfig
from jobs.scheduler import scheduleJobs


# =========================================================================
# Main
# =========================================================================

# Example for Rest API service
class BoilerplateRestMicroservice(AbstractMicroservice):
    """
    Microservice class defining the Boilerplate microservice.
    """

    def __init__(
        self,
    ) -> None:
        super(BoilerplateRestMicroservice, self).__init__(
            serviceEndpointConfiguration=ServiceEndpointConfiguration(
                host=config.endpoint.internalHost,
                port=config.endpoint.internalPort,
            ),
            jwtSecretKey=config.secrets.jwtSecret,
            databaseConfiguration=DatabaseConfiguration(
                name=config.mysql.mySQLDBName,
                host=config.mysql.mySQLHost,
                port=config.mysql.mySQLPort,
                user=config.mysql.mySQLUsername,
                password=config.mysql.mySQLPassword,
            ),
            redisConfigurations=[
                RedisConfiguration(
                    name=redisName,
                    host=redisEndpoint[0],
                    port=redisEndpoint[1],
                    dbNumber=redisEndpoint[2],
                )
                for redisName, redisEndpoint in config.redis.redisEndpoints.items()
            ],
            logRedactionPatterns=[],
            logMessageFilterFunc=BoilerplateRestMicroservice.filterLogMessage,
        )

    def onAfterInitialization(self) -> None:
        """
        This is called just after the initialization of the microservice.
        """

        # Schedule the periodic jobs
        scheduleJobs()

    @staticmethod
    def filterLogMessage(
        message: str,
    ) -> bool:
        """
        Returns whether to show log message or not.
        """
        return not (
            "/ping " in message or
            # Hide successful GET messages
            ('"GET ' in message and 'HTTP/1.1" 200' in message)
        )


# Example for task queue service
class BoilerplateTaskQueueMicroservice(TaskQueueMicroservice):
    """
    Microservice class defining the Boilerplate task queue microservice.
    """

    def __init__(
        self,
    ) -> None:
        super(BoilerplateTaskQueueMicroservice, self).__init__(
            celeryConfiguration=celery.configuration,
            databaseConfiguration=None,
            redisConfigurations=[],
            logRedactionPatterns=None,
            logMessageFilterFunc=None
        )


# Example for schedule service
class BoilerplateScheduleMicroservice(ScheduleMicroservice):
    """
    Microservice class defining the Boilerplate schedule microservice.
    """

    def __init__(
        self,
    ) -> None:
        super(BoilerplateScheduleMicroservice, self).__init__(
            celeryConfiguration=celery.configuration,
            # Info - https://docs.celeryproject.org/en/stable/userguide/periodic-tasks.html
            celerySchedules={
                'add-every-two-minutes-maintenance-job': {
                    'task':     'boilerplate.schedules.projectMaintenance',
                    # Execute every 2 minutes - more info
                    'schedule': crontab(minute='*/2'),
                    'args':     ('Maintenance', 16),
                },
            }
        )


microserviceClass = getExecutionServiceByConfig(
    restService=BoilerplateRestMicroservice,
    queueService=BoilerplateTaskQueueMicroservice,
    scheduleService=BoilerplateScheduleMicroservice
)
