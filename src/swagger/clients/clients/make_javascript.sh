#!/bin/bash

rm -fr javascript/
# Download from https://s3.amazonaws.com/perfpie-static-assets/tools/openapi-generator-cli-3.3.4.jar
java -jar ~/Downloads/openapi-generator-cli-3.3.4.jar generate -i ../boilerplate.yaml -l javascript --additional-properties usePromises=true --additional-properties useES6=false -o ./javascript/

cp -fr javascript/* ../../../../growl/local_npm_packages/boilerplate/

