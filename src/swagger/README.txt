In case of changing the schema:

0. Make changes in the appropriate Boilerplate model if necessary.
1. Change version
2. Rebuild the JavaScript client: make_javascript.sh
3. `yarn install` in Crackle
4. Change the GraphQL schema in Crackle if relevant.