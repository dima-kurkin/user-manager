""" 
Account related logic functions 
"""

# Copyright 2017-2019 (c) Mayple. All rights reserved.
# Copying and/or distribution of this file is prohibited.

from sqlalchemy.orm import Session

from common.mysql import createEntity
from models.accounts import Account


# ======================================================================================================================
# Public Members
# ======================================================================================================================


def createAccountEntity(
    session: Session,
    accountCreate: dict,
) -> Account:
    """
    Creates a Account entity

    :param session:
    :param accountCreate: Account creation dict
    :param associateUserToAccount:
    :return: Account entity
    """

    # ... you can do validation stuff before ...

    account = createEntity(session, Account, accountCreate)
    session.add(account)

    # ... you can do various handling and notification code after ...

    return account
