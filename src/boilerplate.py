#!/usr/bin/env python

# Copyright 2017-2019 (c) Mayple. All rights reserved.
# Copying and/or distribution of this file is prohibited.

"""
Standard command-line entrance point for the microservice.
"""

# This line must always be the first line executed in the microservice

import gevent.monkey


# This line must always be the second line executed in the microservice
gevent.monkey.patch_all()

# =======================================================================================================================
# Main entry point for the microservice
# =======================================================================================================================

if __name__ == "__main__":
    # Import all extra code here, to make sure the gevent code always runs before anything else - even after
    # automatic reformatting and re-ordering of imports
    import os
    import common.utils.entrypoint

    # The name of the file decides the name of the microservice, system wide!
    currentFileName = os.path.splitext(os.path.basename(__file__))[0]

    common.utils.entrypoint.startMicroservice(
        targetName=currentFileName,
    )
