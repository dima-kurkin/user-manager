"""
Tests initialization - this file is loaded first, before any other tests file.
"""

# Copyright 2017-2019 (c) Mayple. All rights reserved.
# Copying and/or distribution of this file is prohibited.

# =======================================================================================================================
# Process Setup
# =======================================================================================================================

import os
import sys

# Include the src files in import path
# adding ../src to path array to make Python able to import packages common.*
projectPath = sys.path[0]
sys.path.append(os.path.join(projectPath, 'src'))

# Set the environment name to be 'tests'
os.environ['PERFPIE_ENVIRONMENT'] = 'tests'

# =======================================================================================================================
# Microservice Setup
# =======================================================================================================================

# We only import it here, to not have anything loaded before we set the environment
from common.utils.entrypoint import setupTestsEnvironment
from config.mysql import mySQLDBName


# Microservice name is:
microserviceName = "boilerplate"

# Setup the tests environment
setupTestsEnvironment(
    projectPath=projectPath,
    microserviceName=microserviceName,
    overrideMicroserviceDatabaseSchemaName=mySQLDBName
)
