from pytest_bdd import given

from tests.utilities.fixtures import truncateDatabase


@given('Reset the service data')
def reset_service():
    truncateDatabase()