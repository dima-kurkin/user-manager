import json
from typing import Any
from typing import Dict

from flask.testing import FlaskClient
from tests.utilities.api import _api_create_token


_countryUrl = '/boilerplate/accounts'

def api_getAccountList(httpTestClient: FlaskClient, accessToken: str=None)->Any:
    if accessToken is None:
        accessToken = _api_create_token()

    return httpTestClient.get(
        _countryUrl,
        headers={'Authorization': f'Bearer {accessToken}'},
        content_type='application/json',
    )

def api_createAccount(httpTestClient: FlaskClient, accountDict: Dict, accessToken: str=None)->Any:
    if accessToken is None:
        accessToken = _api_create_token()

    return httpTestClient.post(
        _countryUrl,
        headers={'Authorization': f'Bearer {accessToken}'},
        data=json.dumps(accountDict),
        content_type='application/json',
    )

def api_updateAccount(httpTestClient: FlaskClient,accountId: str, accountUpdate: Dict, accessToken: str=None)->Any:
    if accessToken is None:
        accessToken = _api_create_token()

    return httpTestClient.put(
        f"{_countryUrl}/{accountId}",
        headers={'Authorization': f'Bearer {accessToken}'},
        data=json.dumps(accountUpdate),
        content_type='application/json',
    )

def api_getAccount(httpTestClient: FlaskClient,accountId: str,accessToken: str=None)->Any:
    if accessToken is None:
        accessToken = _api_create_token()

    return httpTestClient.get(
        f"{_countryUrl}/{accountId}",
        headers={'Authorization': f'Bearer {accessToken}'},
        content_type='application/json',
    )
