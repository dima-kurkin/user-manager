from common.test_helpers.utilities import makeJWTTokenExtended
from models import Enumerations


def _api_create_token(userType: Enumerations.UserType = None):
    return makeJWTTokenExtended(userType or Enumerations.UserType.ADMIN_USER,)
