# noinspection SqlDialectInspection
from common.mysql import MySQLDatabase
from common.redis_db import RedisDatabase
from common.test_helpers import getEntityTableName
from models.accounts import Account


# ======================================================================================================================
# General helping functions
# ======================================================================================================================

_createdStoreProc = None


def clearCache():
    redisProxy = RedisDatabase.instance().getRedisProxy()
    redisProxy and redisProxy.flushall()


def truncateDatabase(entityCls=None):
    """ Clears all of the tables in the database """
    # Also clear the Redis cache
    clearCache()
    # safety check if tests are trying to clear the DB without having a DB at all
    # for example, in unit testing..
    try:
        with MySQLDatabase.instance().scopedSession():
            pass
    except Exception:
        return True

    with MySQLDatabase.instance().scopedSession() as session:
        if entityCls:
            session.execute('SET FOREIGN_KEY_CHECKS = 0')
            session.execute(f'TRUNCATE table {getEntityTableName(entityCls)}')
            session.execute('SET FOREIGN_KEY_CHECKS = 1')
        else:
            global _createdStoreProc
            conn = session.connection().connection
            cursor = conn.cursor()  # get mysql db-api cursor
            if not _createdStoreProc:
                createProcedureSql = f"""CREATE PROCEDURE truncateAllTables()
                BEGIN
                SET FOREIGN_KEY_CHECKS=0;
                TRUNCATE table {getEntityTableName(Account)};
                ALTER TABLE {getEntityTableName(Account)} AUTO_INCREMENT = 1;                
                SET FOREIGN_KEY_CHECKS=1;
                END"""
                cursor.execute(createProcedureSql)
                _createdStoreProc = True
            executeSql = 'CALL truncateAllTables();'
            cursor.execute(executeSql)
