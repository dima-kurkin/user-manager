# Copying and/or distribution of this file is prohibited.
import pytest
from tests.utilities.fixtures.helpers import truncateDatabase
# ======================================================================================================================
# General purpose fixtures
# ======================================================================================================================


@pytest.fixture(scope='module', autouse=True)
def setupFunctionalTestingModuleEnvironment(
    initMicroservice
):
    """
    Prepare each testing module automatically (see decorator params)
    :param initMicroservice: Fixture for loading up the systems (happens only once)
    """
    ##################################
    # setUp before every test module #
    ##################################
    truncateDatabase()
    yield {}
    ####################################
    # tearDown after every test module #
    ####################################
    # Undo possible changes
    truncateDatabase()


@pytest.fixture(scope='function', autouse=True)
def setupUnitTestingModuleEnvironment():
    """
    Prepare each unit testing function
    To be used manually
    """
    #########################################
    # setUp before every unit test function #
    #########################################
    truncateDatabase()
    yield {}
    ###########################################
    # tearDown after every unit test function #
    ###########################################
    # Undo possible changes
    truncateDatabase()
