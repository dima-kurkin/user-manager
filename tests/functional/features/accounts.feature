Feature: Accounts
  Background:
    Given Reset the service data
    When User creates account "mayple-1"
    When User creates account "mayple-2"

  Scenario Outline: Adminstractor get accounts list
    Given Logged-in user is of type <userType>
    When Get list of accounts
    Then Amount of accounts is <accountsNumber>, status code is <status>
    Examples:
      | userType     |accountsNumber | status |
      | ADMIN_USER   |  2            |  200   |
      | SUPPORT_USER |  2            |  200   |
      | NORMAL_USER  |  0            |  403   |


  Scenario: User update accounts information
    When User creates account "mayple-3"
    When User updates the email address of account "mayple-3" to "market@mayple.com"
    Then The email address of account "mayple-3" is equal to "market@mayple.com"