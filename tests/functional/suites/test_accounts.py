from typing import Dict

from flask.testing import FlaskClient
from pytest_bdd import parsers
from pytest_bdd import scenario
from pytest_bdd import scenarios
from pytest_bdd import then
from pytest_bdd import when

from common.test_helpers.utilities import decodeJSON
from common.test_helpers.utilities import makeJWTTokenExtended
from models import Enumerations
from tests.utilities.api.api_access.test_accounts import api_createAccount
from tests.utilities.api.api_access.test_accounts import api_getAccount
from tests.utilities.api.api_access.test_accounts import api_getAccountList
from tests.utilities.api.api_access.test_accounts import api_updateAccount
from tests.utilities.steps import *
# ======================================================================================================================
# Feature
# ======================================================================================================================

# ======================================================================================================================
# Scenario:
# ======================================================================================================================
CONVERTERS = dict(userType=str, status=int)
@scenario( '../features/accounts.feature', 'Adminstractor get accounts list', example_converters=CONVERTERS)
def test_acounts()-> None:
    pass

@given("Logged-in user is of type <userType>")
def setTokenByUserType(testContext: Dict, userType: str)-> None:
    assert userType in [item.name for item in Enumerations.UserType]
    testContext['userToken'] = makeJWTTokenExtended(Enumerations.UserType[userType])


@when('Get list of accounts')
def getAccountList(testContext: Dict, httpTestClient: FlaskClient)-> None:
    response = api_getAccountList(httpTestClient,testContext['userToken'])
    testContext['response'] = {
        'statusCode': response.status_code,
        'body': decodeJSON(response)
    }


@then('Amount of accounts is <accountsNumber>, status code is <status>')
def verifyAccountList(testContext: Dict, httpTestClient: FlaskClient, accountsNumber:int, status:int) -> None:
    assert testContext['response']['statusCode'] == int(status)
    if testContext['response']['statusCode'] in [200,201]:
        assert len(testContext['response']['body']['items']) == int(accountsNumber)

###################################################################
# scenarios:
#   - User update accounts information
###################################################################

scenarios( '../features/accounts.feature')

@when(parsers.parse("User creates account {accountName}"))
def createAccount(testContext: Dict, httpTestClient: FlaskClient,accountName: str) -> None:
    account = {
        "emailAddress": f"{accountName}@mayple.com",
        'presentableName': accountName,
    }
    response = api_createAccount(httpTestClient,accountDict=account)
    assert response.status_code == 201
    body = decodeJSON(response)
    # Add dict map: account -> uuid, if not exist
    if not hasattr(testContext,'accountNameToUuid'):
        testContext['accountNameToUuid'] = {}
    # Store the account Uuid for next step
    testContext['accountNameToUuid'][accountName] = body['uuid']

@when(parsers.parse("User updates the email address of account {accountName} to {emailAddress}"))
def updateAccountEmail(testContext: Dict, httpTestClient: FlaskClient,accountName: str,emailAddress: str) -> None:
    accountUpdate = {
        "emailAddress": emailAddress
    }
    accountUuid: str = testContext['accountNameToUuid'][accountName]
    response = api_updateAccount(httpTestClient,accountUuid,accountUpdate)
    assert response.status_code == 200

@then(parsers.parse("The email address of account {accountName} is equal to {emailAddress}"))
def verifyAccountInfo(testContext: Dict, httpTestClient: FlaskClient,accountName: str,emailAddress: str) -> None:
    accountUuid: str = testContext['accountNameToUuid'][accountName]
    response = api_getAccount(httpTestClient,accountUuid)
    assert response.status_code == 200
    body = decodeJSON(response)
    assert body['emailAddress'] == emailAddress
