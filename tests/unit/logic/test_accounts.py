"""
Tests of logic.accounts.
"""

# Copyright 2017-2020 (c) Mayple. All rights reserved.
# Copying and/or distribution of this file is prohibited.

from typing import Dict

import pytest

from common.mysql import MySQLDatabase
from logic.accounts import createAccountEntity


# ======================================================================================================================
# Tests
# ======================================================================================================================


# noinspection PyUnusedLocal
def test_createAccountEntity(
    initMicroservice,  # Fixture
) -> None:

    # Getting a random company dict
    accountCreateDict = getAccountCreate()

    with MySQLDatabase.instance().scopedSession() as session:
        # Throw error - Normal user + associateUserToAccount is False
        createAccountEntity(
            session=session,
            accountCreate=accountCreateDict,
        )
        session.flush()

        with pytest.raises(Exception, match="argument of type 'int' is not iterable"):
            # noinspection PyTypeChecker
            createAccountEntity(
                session=session,
                accountCreate=15,
            )


def getAccountCreate() -> Dict:
    return {
        'emailAddress':    'Random@account.com',
        'presentableName': 'Random Account',
    }
