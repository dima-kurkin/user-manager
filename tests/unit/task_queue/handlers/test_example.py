import pytest


@pytest.mark.skip(reason="currently doesn't work need RabbitMQ docker")
def test_verify_handle_example(celery_app, celery_worker):
    celery_app.send_task(
        correlation_id='11',
        name="boilerplate.handle.example",
        args=["function vale", {"name": "test-msg", "id": 165}]
    )
