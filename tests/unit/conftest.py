# Copyright 2016-2020 (c) Mayple. All rights reserved.
# Copying and/or distribution of this file is prohibited.

# ======================================================================================================================
# Functional Testing Fixtures Only
# ======================================================================================================================

# To have database truncating between tests - check this out in Fizzle
# noinspection PyUnresolvedReferences
from tests.utilities.fixtures import setupUnitTestingModuleEnvironment
