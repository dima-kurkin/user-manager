"""
Global conftest.py file, test configuration for all tests.
More info here:
https://docs.pytest.org/en/latest/fixture.html#conftest-py-sharing-fixture-functions
"""

# Copyright 2017-2019 (c) Mayple. All rights reserved.
# Copying and/or distribution of this file is prohibited.

import pytest
from mock import patch

from common.test_helpers.fixtures import connexionApp
from common.test_helpers.fixtures import httpTestClient
from common.test_helpers.fixtures import initMicroservice
from common.test_helpers.fixtures import initMySqlServer
from common.test_helpers.fixtures import mySQLTestContainerFixture
from common.test_helpers.fixtures import redisTestContainerFixture
from common.test_helpers.fixtures import testContext


# =======================================================================================================================
# py.test Setup
# =======================================================================================================================

# GJ Tomer! --->
# pytest magic! DO NOT TOUCH THIS
# TL;DR - pytest does some magical shit with assertion to allow
#         pretty printing of assert failures. It is limited by
#         default to be turned on only on test_*.py files. Once
#         test steps / helpers are extracted into separated python
#         modules (e.g steps.py, helpers.py etc.) then this feature
#         is turned off by default and the assert output is normal
#         which is without the "prettifying" of pytest.
#         What this line does is telling pytest to turn it on for all
#         assertions under tests.utilities package using python
#         magic thus moving / touching it will break it for sure
#
# For further information please check:
# 1. Official documentation of this feature:
# https://docs.pytest.org/en/3.7.0/writing_plugins.html#assertion-rewriting
# 2. Behind the scene explanation: http://pybites.blogspot.com/2011/07/behind-scenes-of-pytests-new-assertion.html
pytest.register_assert_rewrite('tests.utilities')

# ======================================================================================================================
# Global Mocking
# ======================================================================================================================

# retry() is a decorator that applies re-calling a function (with retry settings defined in *args, **kwargs).
# It is mostly used on interaction with external microservice APIs due to potential normal connection hiccups etc.
# but is pointless in the tests as there will be 100% connection hiccups as there are no external microservices
# running during the functional tests of this microservice.
# What we do here is globally mocking the decorator with an empty lambda that return the decorated function itself
# thus skipping the retry mechanism and running the external microservice API as-is (which is the expected
# behavior because the external microservice API is mocked internally in the tests which also makes sense as you don't
# need to handle connection hiccups when calling internal code)
patch('retrying.retry', side_effect=lambda *dargs, **dkw: dargs[0])

# Mocks to export, to be used in the system. We just include them here so auto-formatting of imports won't remove
# unused imports, but it also helps organize
mocksToExport = {
}

# ======================================================================================================================
# Fixtures
# ======================================================================================================================

# Fixtures loaded here are later usable by other tests automatically as this file
# gets imported first before all tests under it
# https://stackoverflow.com/questions/34466027/in-pytest-what-is-the-use-of-conftest-py-files

# Fixtures to export, to be used in the system. We just include them here so auto-formatting of imports won't remove
# unused imports, but it also helps organize
fixturesToExport = {
    mySQLTestContainerFixture,  # Load the mySQL docker
    redisTestContainerFixture,  # Load the Redis docker
    connexionApp,
    initMicroservice,
    initMySqlServer,
    httpTestClient,
    testContext,
}
