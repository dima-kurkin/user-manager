To create a new microservice repository, first choose a name, from this list:

https://en.wikipedia.org/wiki/List_of_onomatopoeias
https://en.wiktionary.org/wiki/Category:English_onomatopoeias

Must be the name of a sound, relevant to the domain of the microservice! Then:

```bash
MICROSERVICE_NAME=<name>
git clone -o $MICROSERVICE_NAME -b master --single-branch git@bitbucket.org:perfpie/boilerplate.git $MICROSERVICE_NAME
cd $MICROSERVICE_NAME
rm -fr .git
rm -fr .gitmodules
rm -fr common/
git init
git submodule add git@bitbucket.org:perfpie/common.git
git submodule update --init
git add .
git commit -am "Initial commit for microservice $MICROSERVICE_NAME based on boilerplate."
git checkout -B develop
```

Then go to Bitbucket and create a new repository for this microservice.
Make sure to import the existing repository instead of creating a new one with README, etc.
Rename all files that are called boilerplate to the name of the new microservice.
Search/replace all BOILERPLATE, boilerplate, Boilerplate etc in project.
Set a Redis DB number and default port.

To install:

```bash
make setup-venv
```

To run:

```bash
make run
```

To run in PyCharm:

1. Remember to Mark .venv/ and common/ as "Excluded" in PyCharm, and set
src/ as the sources root.
2. Create a Python run configuration with details:
    Script path: ..../boilerplate/src/boilerplate.py
    Working Directory: ..../boilerplate/src/
    Env vars: PYTHONUNBUFFERED=1;PERFPIE_ENVIRONMENT=dorky

To run tests:

Create a Python tests pytest run configuration, with details:
  Target: Custom
  Additional Arguments: tests/
  Working Directory: ..../boilerplate

